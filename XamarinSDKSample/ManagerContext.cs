﻿using System;
using Foundation;
using NXBeaconSDK;
using UIKit;

namespace XamarinSDKSample
{
    public class ManagerContext
    {
        private static ManagerContext instance;
        private NXProximityManager manager;

        public static ManagerContext Instance
        {
            get
            {
                if (instance == null){
                    instance = new ManagerContext();
                }

                return instance;
            }
        }

        private ManagerContext()
        {
            manager = new NXProximityManager("appuser", "banana", new NSUrl("https://demoportal.wearenexen.io/app/"), NSLocale.CurrentLocale);
        }

        public void StartManager(Action onComplete)
        {
            var center = NSNotificationCenter.DefaultCenter;
            center.AddObserver(new NSString("NXProximityManagerTriggerContentNotification"), ContentTriggering, manager);
            manager.Start((NSError obj) =>
            {
                onComplete();
            });
        }

        public void PerformFetch(Action<UIBackgroundFetchResult>  completionHandler)
        {
            manager.PerformFetchWithCompletionHandler(completionHandler);
        }

        public void HandleEventsForBackgroundURLSession(string sessionIdentifier, Action completionHandler){
            manager.HandleEventsForBackgroundURLSession(sessionIdentifier, completionHandler);
        }

        private void ContentTriggering(NSNotification notification)
        {
            var beacon = NXProximityManager.ProximityZoneFromNotification(notification);

            if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Background)
            {
                UILocalNotification localNotification = new UILocalNotification();
                localNotification.FireDate = NSDate.FromTimeIntervalSinceNow(1);
                localNotification.AlertBody = beacon.Content.Message;
                localNotification.UserInfo = new NSDictionary("beaconIdentifier", beacon.Identifier);
                UIApplication.SharedApplication.ScheduleLocalNotification(localNotification);
            }
            else
            {
                /* HANDLE ACTUAL CONTENT SHOWING HERE */

                UIAlertView _error = new UIAlertView("Content Message", beacon.Content.Message, null, "Ok", null);

                _error.Show();
            }
        }
    }
}
